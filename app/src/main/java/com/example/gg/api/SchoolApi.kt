package com.example.gg.api

import com.example.gg.data.SchoolEntity
import com.example.gg.data.SchoolSatResponse
import io.reactivex.Observable
import retrofit2.http.GET


interface SchoolApi {

    @GET("resource/s3k6-pzi2.json")
    fun getNySchoolList(): Observable<List<SchoolEntity>>

    @GET("resource/f9bf-2cp4.json")
    fun getSchoolSat(): Observable<List<SchoolSatResponse>>
}