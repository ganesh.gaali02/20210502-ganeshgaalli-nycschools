package com.example.gg.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gg.data.SchoolEntity
import com.example.gg.data.SchoolSatResponse
import com.example.gg.repo.SchoolRepository
import com.example.gg.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val repository: SchoolRepository) : ViewModel(){

    private val disposable = CompositeDisposable()

    val schoolListRep = MutableLiveData<Resource<List<SchoolEntity>>>()
    val schoolSatRep = MutableLiveData<Resource<List<SchoolSatResponse>>>()

    fun getSchoolList() {
        disposable.add(repository.getSchoolList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { schoolListRep.value = Resource.loading() }
            .subscribe({ result -> schoolListRep.value = Resource.success(result) }
            ) { error -> schoolListRep.value = Resource.error(error.message) }
        )
    }

    fun getSchoolSat() {
        disposable.add(repository.getSchoolSat()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { schoolSatRep.value = Resource.loading() }
            .subscribe({ result -> schoolSatRep.value = Resource.success(result) }
            ) { error -> schoolSatRep.value = Resource.error(error.message) }
        )
    }

    override fun onCleared() {
        super.onCleared()
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
    }

}