package com.example.gg.ui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ItemSchool(val code: String, val title: String) : Parcelable