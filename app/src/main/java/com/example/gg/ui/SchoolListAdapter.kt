package com.example.gg.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gg.databinding.ItemSchoolBinding

class SchoolListAdapter(private val listener: SchoolItemListener) :
    RecyclerView.Adapter<SchoolViewHolder>() {

    private val data = emptyList<ItemSchool>().toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding =
            ItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) =
        holder.bind(data[position])

    interface SchoolItemListener {
        fun onItemClicked(itemSchool: ItemSchool)
    }

    fun updateData(items: List<ItemSchool>) {
        this.data.clear()
        this.data.addAll(items)
        notifyDataSetChanged()
    }
}

class SchoolViewHolder(
    private val itemBinding: ItemSchoolBinding,
    private val listener: SchoolListAdapter.SchoolItemListener
) : RecyclerView.ViewHolder(itemBinding.root), View.OnClickListener {

    private lateinit var itemSchool: ItemSchool

    init {
        itemBinding.root.setOnClickListener(this)
    }

    fun bind(item: ItemSchool) {
        this.itemSchool = item
        itemBinding.tvSchoolName.text = item.title
    }

    override fun onClick(v: View?) {
        listener.onItemClicked(itemSchool)
    }
}