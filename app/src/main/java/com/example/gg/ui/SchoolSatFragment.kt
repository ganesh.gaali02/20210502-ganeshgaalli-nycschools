package com.example.gg.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gg.R
import com.example.gg.databinding.FragmentSchoolListBinding
import com.example.gg.databinding.FragmentSchoolSatBinding
import com.example.gg.util.Resource
import com.example.gg.vm.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SchoolSatFragment : Fragment() {

    private lateinit var binding: FragmentSchoolSatBinding
    private val viewModel: SchoolViewModel by viewModels()

    private val args: SchoolSatFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolSatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
    }

    override fun onStart() {
        super.onStart()
        viewModel.getSchoolSat()
    }

    @SuppressLint("StringFormatInvalid")
    private fun setupObservers() {
        viewModel.schoolSatRep.observe(viewLifecycleOwner, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.pb.isVisible = false
                    val item = it.data?.find { item -> item.dbn == args.itemSchool.code }
                    if (item == null) {
                        binding.itemNotFound.isVisible = true
                        binding.itemNotFound.text =
                            getString(R.string.item_found, args.itemSchool.code)
                    } else {
                        binding.tvSchoolName.isVisible = true
                        binding.tvSatMathTacker.isVisible = true
                        binding.tvSatTacker.isVisible = true
                        binding.tvSatWritTacker.isVisible = true
                        binding.tvSchoolName.text =
                            getString(R.string.school_name, item.school_name)
                        binding.tvSatMathTacker.text =
                            getString(R.string.sat_math_avg_score, item.sat_math_avg_score)
                        binding.tvSatTacker.text =
                            getString(R.string.sat_critical_reading, item.num_of_sat_test_takers)
                        binding.tvSatWritTacker.text =
                            getString(R.string.at_writing_avg_score, item.sat_writing_avg_score)
                    }

                }
                Resource.Status.ERROR -> {
                    binding.pb.isVisible = false
                }
                Resource.Status.LOADING -> {
                    binding.pb.isVisible = true
                }
            }
        })
    }
}