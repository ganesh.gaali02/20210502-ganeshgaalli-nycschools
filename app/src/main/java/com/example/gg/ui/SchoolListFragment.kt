package com.example.gg.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gg.databinding.FragmentSchoolListBinding
import com.example.gg.util.Resource
import com.example.gg.vm.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SchoolListFragment : Fragment(), SchoolListAdapter.SchoolItemListener {

    private lateinit var binding: FragmentSchoolListBinding
    private val viewModel: SchoolViewModel by viewModels()
    private lateinit var adapter: SchoolListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    override fun onStart() {
        super.onStart()
        viewModel.getSchoolList()
    }

    private fun setupObservers() {
        viewModel.schoolListRep.observe(viewLifecycleOwner, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.pbList.isVisible = false
                    val itemSeatGeekList = it.data?.map { item ->
                        ItemSchool(
                            code = item.dbn,
                            title = item.school_name
                        )
                    } ?: emptyList()
                    adapter.updateData(itemSeatGeekList)
                }
                Resource.Status.ERROR -> {
                    binding.pbList.isVisible = false
                }
                Resource.Status.LOADING -> {
                    binding.pbList.isVisible = true
                }
            }
        })
    }

    private fun setupRecyclerView() {
        adapter = SchoolListAdapter(this)
        binding.apply {
            rvSchoolList.layoutManager = LinearLayoutManager(requireContext())
            rvSchoolList.adapter = adapter
        }
    }

    override fun onItemClicked(itemSchool: ItemSchool) {
        val directions = SchoolListFragmentDirections.goToSat(itemSchool)
        findNavController().navigate(directions)
    }
}