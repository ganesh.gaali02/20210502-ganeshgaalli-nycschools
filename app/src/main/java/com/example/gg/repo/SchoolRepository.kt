package com.example.gg.repo

import com.example.gg.api.SchoolApi
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val service: SchoolApi) {
    fun getSchoolList() = service.getNySchoolList()
    fun getSchoolSat() = service.getSchoolSat()
}