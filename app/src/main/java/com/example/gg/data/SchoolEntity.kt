package com.example.gg.data

import com.google.gson.annotations.SerializedName

data class SchoolEntity(
    @SerializedName("dbn") val dbn: String,
    @SerializedName("school_name") val school_name: String
)
